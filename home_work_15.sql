﻿-- USE shop;

-- Вывести все типы товаров, для которых нет ни одного товара в нашем интернет магазине
-- сначала добавим такой товар
INSERT INTO product_type (name) VALUES ('Носки');
-- и сделаем нужный запрос
SELECT * FROM product_type LEFT JOIN product ON product.product_type_id = product_type.id;
-- этот запрос покажет  значения только из product_type с нулевым значением
SELECT product_type.* FROM product_type LEFT JOIN product ON product.product_type_id = product_type.id  WHERE product.id IS NULL;

-- Вывести информацию обо всех товарах, которые не попали ни в один из заказов
-- объединим все наши 3 таблицы с помощью INNER JOIN
SELECT * FROM `order`
	INNER JOIN order_products ON order_products.order_id = `order`.id
	INNER JOIN product ON order_products.product_id = product.id;
-- нас интересуют все товары, которые не попали ни в один из заказов, т.е. `order`id=NULL
SELECT * FROM `order` 
	INNER JOIN order_products ON order_products.order_id = `order`.id
	RIGHT JOIN product ON order_products.product_id = product.id
	WHERE `order`.id IS NULL;
-- нас интересуют только товары, их и выведем:
SELECT product.* FROM `order` 
	INNER JOIN order_products ON order_products.order_id = `order`.id
	RIGHT JOIN product ON order_products.product_id = product.id
	WHERE `order`.id IS NULL;
