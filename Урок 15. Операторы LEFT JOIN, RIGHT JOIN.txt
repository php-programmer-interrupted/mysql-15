Урок 15. Операторы LEFT JOIN, RIGHT JOIN

00:00	-	Разбор домашнего задания №14
01:58	-	Техника формирования запроса из нескольких таблиц
03:00	-	Использование псевдонимов для столбцов
03:35	-	Допустим перед нами стоит задача:
			Найти категорию товаров, для которой нет ни одной единицы товара.
			В данном случае нам поможет LEFT JOIN, это объединение, которое предполагает, что
			в него попадают все строки из левой таблицы A и те строки из таблицы B, у которых
			есть пересечение с таблицей A.
			Недостающие строчки заполняются NULL.
05:00	-	Пример LEFT JOIN:
			SELECT * FROM category
			 LEFT JOIN product ON product.category_id = category.id;
05:50	-	Исходя из формулировки задачи нам нужно вычленить строчку со значением NULL.
			SELECT * FROM category
			 LEFT JOIN product ON product.category_id = category.id
			  WHERE product.id IS NULL;
06:20	-	Избавляемся от лишних столбцов:			  
			SELECT category.* FROM category
			 LEFT JOIN product ON product.category_id = category.id
			  WHERE product.id IS NULL;
06:54	-	Существует аналогичная LEFT JOIN директива - RIGHT JOIN
			Делает все то же самое, только в результаты запроса должны попасть все строки из той
			таблицы, которая находится справа
			Если мы поменяем таблицы местами, то получим такой же запрос, но с использованием RIGHT JOIN
			SELECT category.* FROM product
			 RIGHT JOIN category ON product.category_id = category.id
			  WHERE product.id IS NULL;
07:20	-	Домашнее задание.
			  